﻿	using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndLevelTrigger : MonoBehaviour {

	private LevelManager levelManager;
	private PlayerControllerButterfly player;
	private Wall wall;

	void Start() {
		levelManager = FindObjectOfType<LevelManager> ();
		player = FindObjectOfType<PlayerControllerButterfly> ();
		wall = FindObjectOfType<Wall> ();
	}

	void OnTriggerEnter2D(Collider2D other) {
		if (other.gameObject.layer == 8) {
			player.Stop ();
			wall.Stop ();
			levelManager.Invoke ("FinishedLevel", 3);
		}
	}
}
