﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelManager : MonoBehaviour {

	public float score;
	public Text scoreboard;
	public Text cycles;
	public float scoreThreshold;

	public void Collected(float points) {
		score += points;
		scoreboard.text = score+"/"+scoreThreshold;
	}

    void Start()
    {
		score = Cycle.score;
		if(cycles)
			cycles.text = "Cycle " + Cycle.cycle;
		if(scoreboard)
			scoreboard.text = score + "/" + scoreThreshold;
    }

	public void NewGame(string name) {
		score = 0;
		Cycle.cycle = 1;
		Cycle.baseSpeed = 3;
		Cycle.score = 0;
		LoadLevel (name);
	}

    public void LoadLevel(string name)
    {
        Debug.Log("Level load requested for: " + name);
        SceneManager.LoadScene(name,LoadSceneMode.Single);
    }

    public void Quit()
    {
        Debug.Log("Quit requested");
        Application.Quit();
    }

    public void FinishedLevel()
    {
		if (score < scoreThreshold) {
			Lose ();
		}
		else {
			Cycle.cycle++;
			score -= scoreThreshold;
			Cycle.score = score;
			Time.timeScale = Cycle.baseSpeed + (Cycle.cycle - 1) * Cycle.speedPerCycle;
			SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex);
		}
    }

	public void Lose()
	{
		Cycle.score = score;
		LoadLevel ("03 Lose");
	}

	public void Win()
	{
		LoadLevel ("03 Win");	
	}

}
