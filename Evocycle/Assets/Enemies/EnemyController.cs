﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour {

	void OnCollisionEnter2D(Collision2D collision) {
		Debug.Log ("Colliding");
	}

	public void TakeDamage(float damage){
		gameObject.GetComponent<Rigidbody2D> ().bodyType = RigidbodyType2D.Dynamic;
		gameObject.layer = 12;
		gameObject.GetComponent<Rigidbody2D> ().AddForce (new Vector2 (1f, 1f));
	}
}
	