﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour {

	private Text text;

	void Start () {
		text = GetComponent<Text> ();
	}

	public void Set(int value) {
		text.text = "Score: " + value;
	}
}
