﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shredder : MonoBehaviour {

	void OnTriggerEnter2D(Collider2D collider) {
		if ((collider.gameObject.layer == 11) || (collider.gameObject.layer == 9)) {
			GameObject.Destroy (collider.gameObject);
		}
	}
}
