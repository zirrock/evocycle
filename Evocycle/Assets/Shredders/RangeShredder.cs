﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangeShredder : MonoBehaviour {

	public GameObject player;
	private float offset;

	void Start() {
		offset = transform.position.x - player.transform.position.x;
	}

	void Update() {
		transform.position = new Vector3(offset + player.transform.position.x, transform.position.y, transform.position.z);
	}

	void OnTriggerEnter2D(Collider2D collider) {
		if (collider.gameObject.layer == 11) {
			GameObject.Destroy (collider.gameObject);
		}
	}
}
