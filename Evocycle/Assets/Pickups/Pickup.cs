﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickup : MonoBehaviour {

	private LevelManager levelManager;

	public int value;

	void Start () {
		levelManager = GameObject.FindObjectOfType<LevelManager> ();
	}

	public void Collect() {
		levelManager.Collected (value);
		GameObject.Destroy (this.gameObject);
	}
}
