﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControllerButterfly : PlayerController {

	public float frontalVelocity = 2f;

	private Rigidbody2D rb;

	void Start () {
		rb = GetComponent<Rigidbody2D> ();
	}

	void OnTriggerEnter2D(Collider2D collider) {
		if (collider.gameObject.layer == 10) {
			collider.gameObject.GetComponent<Pickup> ().Collect ();
		}
	}

	public void Stop() {
		frontalVelocity = 0f;
	}

	public float vertical; // TODO remove
	public float horizontal;

	void Update () 
	{
		vertical = Input.GetAxisRaw ("Vertical");
		horizontal = Input.GetAxisRaw ("Horizontal");
		if (vertical != 0)
			vertical /= Mathf.Abs(vertical);
		if (horizontal != 0)
			horizontal /= Mathf.Abs(horizontal);

		float direction;

		if (horizontal == 1) {
			if (vertical == 1)
				direction = 45;
			else if (vertical == 0)
				direction = 0;
			else
				direction = -45;
		} else if (horizontal == 0) {
			if (vertical == 1)
				direction = 90;
			else if (vertical == 0)
				direction = -1;
			else
				direction = -90;
		} else {
			if (vertical== 1)
				direction = 135;
			else if (vertical == 0)
				direction = 180;
			else
				direction = -135;
		}
		if(!(direction == -1))
			transform.rotation = Quaternion.Euler (0, 0, direction);
		//transform.Rotate (new Vector3 (0, 0, Time.deltaTime * rotationMultiplier * -Input.GetAxisRaw ("Horizontal")));
		Vector3 origin = new Vector3 (frontalVelocity, 0, 0);
		Vector3 result = transform.rotation * origin;
		rb.velocity = result;
	}
}
