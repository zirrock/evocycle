﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

	public LevelManager levelManager;

	public void TakeDamage() {
		levelManager.Lose ();
	}
}
