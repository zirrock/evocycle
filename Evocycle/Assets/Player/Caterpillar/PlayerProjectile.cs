﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerProjectile : MonoBehaviour {

	public float velocity;
	public float damage = 1f;

	void Start () {
		GetComponent<Rigidbody2D> ().velocity = new Vector2(velocity, 0f);
	}


	void OnCollisionEnter2D(Collision2D collision) {
		Debug.Log (collision.collider.gameObject.layer);
		if (collision.collider.gameObject.layer == 9) {
			collision.collider.gameObject.GetComponent<EnemyController> ().TakeDamage (damage);
		}
		GameObject.Destroy (gameObject);
	}
}
