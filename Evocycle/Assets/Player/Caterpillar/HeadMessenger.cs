﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeadMessenger : MonoBehaviour {

	public Transform projectile;

	void OnCollisionEnter2D(Collision2D collision) {
		if (collision.collider.gameObject.tag == "Enemy")
			transform.parent.SendMessage ("Collision", collision);
	}

	void OnTriggerEnter2D(Collider2D collider) {
		if (collider.gameObject.layer == 10) {
			collider.gameObject.GetComponent<Pickup> ().Collect ();
		}
	}

	public void Shoot() {
		Instantiate (projectile, transform.position, Quaternion.identity);
	}

}
