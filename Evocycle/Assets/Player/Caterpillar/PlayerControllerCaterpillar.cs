﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControllerCaterpillar : PlayerController {

	public GameObject head;
	public GameObject tail;

	public float distance; // TODO make private
	public float maxBaseLength = 1.4f;
	public float minExtentionLength = 1.5f;
	public float epsilon = 0.05f;
	public float defaultMultiplier;
	public float attackingMultiplier;
	public float contactDamage;

	public int phase = 0; // TODO make private

	public bool isAttacking = false; // TODO make private

	private Rigidbody2D headRb;
	private Rigidbody2D tailRb;

	private Vector2 headPos;
	private Vector2 tailPos;

	private float movement;

	public Vector3 local;

	void Start () 
	{
		headRb = head.GetComponent<Rigidbody2D> ();
		tailRb = tail.GetComponent<Rigidbody2D> ();
		headPos = new Vector2 (head.transform.position.x, head.transform.position.y);
		tailPos = new Vector2 (tail.transform.position.x, tail.transform.position.y);
	}

	public Transform projectile;

	void OnCollisionEnter2D(Collision2D collision) {
		if (collision.collider.gameObject.layer == 9) {
			if (isAttacking)
				collision.collider.gameObject.GetComponent<EnemyController>().TakeDamage(contactDamage);
			else
				TakeDamage ();
		}
	}

	void OnTriggerEnter2D(Collider2D collider) {
		if (collider.gameObject.layer == 10) {
			collider.gameObject.GetComponent<Pickup> ().Collect ();
		}
	}

	public void Shoot() {
		Instantiate (projectile, head.transform.position, Quaternion.identity);
	}
		
	void Update() {
		local = head.transform.localPosition;
		transform.position = head.transform.position - head.transform.localPosition;
	}
		
	void FixedUpdate () {
		headPos = new Vector2 (head.transform.position.x, head.transform.position.y);
		tailPos = new Vector2 (tail.transform.position.x, tail.transform.position.y);
		movement = Input.GetAxisRaw ("Horizontal");

		distance = Vector2.Distance (headPos, tailPos);
		switch (phase) {
		case 0:
			if (movement > epsilon) {
				headRb.bodyType = RigidbodyType2D.Dynamic;
				if (Input.GetButton ("Fire3")) {
					isAttacking = true;
					movement *= attackingMultiplier;
				} else
					movement *= defaultMultiplier;
				headRb.AddForce(new Vector2(movement, 0));
				phase++;
			}
			break;
		case 1:
			if (isAttacking)
				movement *= attackingMultiplier;
			else
				movement *= defaultMultiplier;
			headRb.AddForce(new Vector2 (movement, 0));
			if ((distance > minExtentionLength) && (movement < epsilon)) {
				if (isAttacking) {
					Shoot ();
					phase = 3;
				} else {
					headRb.bodyType = RigidbodyType2D.Static;
					tailRb.bodyType = RigidbodyType2D.Dynamic;
					phase++;
				}
			}
			break;
		case 2:
			if ((distance < maxBaseLength) && (tailRb.velocity.x < epsilon)) {
				headRb.bodyType = RigidbodyType2D.Static;
				tailRb.bodyType = RigidbodyType2D.Static;
				phase = 0;
			}
			break;
		case 3:
			if ((distance < maxBaseLength) && (headRb.velocity.x > -epsilon)) {
				headRb.bodyType = RigidbodyType2D.Static;
				tailRb.bodyType = RigidbodyType2D.Static;
				phase = 0;
				isAttacking = false;
			}
			break;
		}
	}
}
