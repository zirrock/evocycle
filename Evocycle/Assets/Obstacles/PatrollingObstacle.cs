﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatrollingObstacle : MonoBehaviour {

	public float period;
	private float time;
	public Vector2 constraintPoint1;
	public Vector2 constraintPoint2;

	void Start() {
		time = 0;
	}

	void Update() {
		time += Time.deltaTime;
		time %= period;
		if (time < 0.5 * period) {
			transform.position = (constraintPoint2 - constraintPoint1) * (float)(time / (0.5 * period)) + constraintPoint1;
		} else {
			transform.position = (constraintPoint1 - constraintPoint2) * (float)((time-0.5*period) / (0.5 * period)) + constraintPoint2;
		}
	}
}
