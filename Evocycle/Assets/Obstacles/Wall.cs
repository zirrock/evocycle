﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wall : MonoBehaviour {

	private float velocity;
	private PlayerControllerButterfly player;
	public float speedMultiplier;

	void Start () {
		player = FindObjectOfType<PlayerControllerButterfly> ();
		velocity = player.frontalVelocity * speedMultiplier;
		GetComponent<Rigidbody2D> ().velocity = new Vector2(velocity, 0);
	}

	public void Stop() {
		GetComponent<Rigidbody2D> ().velocity = Vector2.zero;
	}

}
